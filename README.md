# DateRangeTrieStorage

Prefix Tree (Trie) specialized for storing and searching for time distributed data. 

This library allows to quickly find values by keys, get data by a date/time range, delete unnecessary data ranges. 

By defining custom storage's configuration, it's possible to optimize the storage for different kind of operations. It can be searched for data within a small time interval or a large one.

The project is in progress.