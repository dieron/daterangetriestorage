﻿namespace DateRangeTrieStorage

module Configurations =
    open TypeDefinitions

    type TrieStorageConfiguration (levelSizes: seq<LevelConfigurationSize>) = 
        member this.Levels = Seq.distinct levelSizes |> Seq.sortBy (fun x -> x) |> Seq.mapi (fun pos x -> {Level = pos; Size = x}) |> Seq.toArray




