﻿module TestPlacements
    
    open DateRangeTrieStorage.TypeDefinitions
    open System

    type TestPlacement(id: Int64, date: DateTime, value: string) =
        interface IPlacement<int64> with
            member val Id = id with get, set
            member val Date = date with get, set
        member this.Value: string = value
        member this.Id = id
        member this.Date = date
            

