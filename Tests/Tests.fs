namespace Tests

open System
open System.Collections
open Microsoft.VisualStudio.TestTools.UnitTesting
open DateRangeTrieStorage.DRTS
open DateRangeTrieStorage.TypeDefinitions
open DateRangeTrieStorage.Configurations
open TestPlacements

[<TestClass>]
type BasicTests () =
    let beginOfTime = DateTime (2019, 01, 01)
    
    [<TestMethod>]
    member this.TestMethodPassing () =
        Assert.IsTrue(true);

    [<TestMethod>]
    member this.CreateDRTS () =
        let sizes = [| Global; D1; H1; M15; M1; S5; S1|]
        let currentStorageConfiguration = TrieStorageConfiguration sizes
        let mutable storage1 = DataRangeTrieStorage<Int64> (currentStorageConfiguration, beginOfTime)

        Assert.IsNotNull(storage1)

    [<TestMethod>]
    member this.AddPlacement () =
        let sizes = [| Global; D1; H1; M15; M1; S5; S1|]
        let currentStorageConfiguration = TrieStorageConfiguration sizes
        let mutable storage1 = DataRangeTrieStorage<Int64> (currentStorageConfiguration, beginOfTime)

        let newPlacement = TestPlacement (34L, new DateTime(2019, 09, 04, 18, 07, 01), "Test Placement")

        storage1.AddPlacement newPlacement

        Assert.IsNotNull(storage1)
        Assert.IsNotNull(newPlacement)

[<TestClass>]
type LogicTests () =
    let beginOfTime = DateTime (2019, 01, 01)
    
    [<TestMethod>]
    member this.FindPlacementDataRange () =
        let sizes = [| Global; D1; H1; M15; M1; S5; S1|]
        let currentStorageConfiguration = TrieStorageConfiguration sizes
        let mutable storage1 = DataRangeTrieStorage<Int64> (currentStorageConfiguration, beginOfTime)

        let newPlacement = TestPlacement (34L, new DateTime(2019, 09, 04, 18, 07, 01), "Test Placement")

        storage1.AddPlacement newPlacement

        let foundPlacement = storage1.FindPlacementInDateRange 34L (new DateTime(2019, 01, 04, 18, 07, 01)) (new DateTime(2019, 12, 04, 18, 07, 01))
        let notFoundPlacement1 = storage1.FindPlacementInDateRange 34L (new DateTime(2019, 01, 04, 18, 07, 01)) (new DateTime(2019, 02, 04, 18, 07, 01))
        let notFoundPlacement2 = storage1.FindPlacementInDateRange 34L (new DateTime(2019, 10, 04, 18, 07, 01)) (new DateTime(2019, 12, 04, 18, 07, 01))

        Assert.IsNotNull storage1
        Assert.IsNotNull newPlacement

        Assert.IsNotNull foundPlacement
        Assert.IsNull notFoundPlacement1
        Assert.IsNull notFoundPlacement2


    [<TestMethod>]
    member this.GetPlacementsDataRange () =
        // let sizes = [| Global; D30; D1; H1; M1; S15|]
        //let sizes = [| Global; D30; D1; H1; M15; M1; S5|]
        let sizes = [| Global; D30; D1; H3; M15; |]
        //let sizes = [| Global; D1; H1; M1 |]
        //let sizes = [| Global; D1; H1; |]
        //let sizes = [| Global; |]
        let currentStorageConfiguration = TrieStorageConfiguration sizes
        let mutable storage1 = DataRangeTrieStorage<Int64> (currentStorageConfiguration, beginOfTime)

        // creating a list of randomly distributed overtheyearplacements
        let secondsPerYear = 365 * 24 * 60 * 60;
        let r = new Random()
        let startDate = new DateTime(2019, 1, 1, 0, 0, 0)
        let count = 100000
        let searchCount = 1000
        let mutable placements = new ResizeArray<TestPlacement>(count)


        let startWork = DateTime.Now

        for i in [0..count-1] do
            let date = startDate.AddSeconds ((r.Next secondsPerYear) |> float)
            let newPlacement = TestPlacement (i |> int64, date, "Test Placement " + date.ToString())

            storage1.AddPlacement newPlacement
            placements.Add newPlacement

        let endWork = DateTime.Now
        let secondsTaken = (endWork - startWork).TotalSeconds
        let speedPerSecond = (float) count / secondsTaken
        printfn "Seconds Taken: %f, Add speed (per second): %f" secondsTaken speedPerSecond



        let startWork = DateTime.Now
        let foundPlacements = storage1.GetPlacementsInDateRange (new DateTime(2019, 1, 1, 0, 0, 0)) (new DateTime(2020, 1, 1, 0, 0, 0)) |> Seq.toArray
        let endWork = DateTime.Now
        let secondsTaken = (endWork - startWork).TotalSeconds
        let speedPerSecond = (float) count / secondsTaken
        printfn "Seconds Taken: %f, Get all in range speed (per second): %f" secondsTaken speedPerSecond


        let startWork = DateTime.Now
        for i in [0..searchCount-1] do
            let placement = placements.[i]
            let startDate = placement.Date.AddSeconds(-10.0)
            let endDate = placement.Date.AddSeconds(10.0)
            let foundPlacements = storage1.GetPlacementsInDateRange startDate endDate |> Seq.toArray
            Assert.IsFalse(Seq.exists (fun (x: IPlacement<int64>) -> x.Date < startDate || x.Date > endDate) foundPlacements)
        let endWork = DateTime.Now
        let secondsTaken = (endWork - startWork).TotalSeconds
        let speedPerSecond = (float) count / secondsTaken
        printfn "Seconds Taken: %f, Get all single placements in range speed (per second): %f" secondsTaken speedPerSecond

        let startWork = DateTime.Now
        for i in [0..searchCount-1] do
            let placement = placements.[i]
            let startDate = placement.Date.AddSeconds(-3600.0)
            let endDate = placement.Date.AddSeconds(3600.0)
            let foundPlacements = storage1.GetPlacementsInDateRange startDate endDate |> Seq.toArray
            Assert.IsFalse(Seq.exists (fun (x: IPlacement<int64>) -> x.Date < startDate || x.Date > endDate) foundPlacements)
        let endWork = DateTime.Now
        let secondsTaken = (endWork - startWork).TotalSeconds
        let speedPerSecond = (float) count / secondsTaken
        printfn "Seconds Taken: %f, Get all single placements in bigger range speed (per second): %f" secondsTaken speedPerSecond


        let startWork = DateTime.Now
        for i in [0..searchCount-1] do
            let placement = placements.[i]
            let startDate = placement.Date.AddSeconds(-10.0)
            let endDate = placement.Date.AddSeconds(10.0)
            let foundPlacement = storage1.FindPlacementInDateRange placement.Id startDate endDate
            Assert.IsTrue(Option.isSome foundPlacement)
        let endWork = DateTime.Now
        let secondsTaken = (endWork - startWork).TotalSeconds
        let speedPerSecond = (float) count / secondsTaken
        printfn "Seconds Taken: %f, Find all single placements in range speed (per second): %f" secondsTaken speedPerSecond

        let startWork = DateTime.Now
        for i in [0..searchCount-1] do
            let placement = placements.[i]
            let startDate = placement.Date.AddSeconds(-3600.0)
            let endDate = placement.Date.AddSeconds(3600.0)
            let foundPlacement = storage1.FindPlacementInDateRange placement.Id startDate endDate
            Assert.IsTrue(Option.isSome foundPlacement)
        let endWork = DateTime.Now
        let secondsTaken = (endWork - startWork).TotalSeconds
        let speedPerSecond = (float) count / secondsTaken
        printfn "Seconds Taken: %f, Find all single placements in biggerrange speed (per second): %f" secondsTaken speedPerSecond

(*
        let startWork = DateTime.Now
        let startDate = (new DateTime(2019, 1, 1, 0, 0, 0))
        let endDate =  (new DateTime(2020, 1, 1, 0, 0, 0))
        for i in [0..searchCount-1] do
            let placement = placements.[i]
            let foundPlacement = storage1.FindPlacementInDateRange placement.Id startDate endDate
            Assert.IsTrue(Option.isSome foundPlacement)
        let endWork = DateTime.Now
        let secondsTaken = (endWork - startWork).TotalSeconds
        let speedPerSecond = (float) count / secondsTaken
        printfn "Seconds Taken: %f, Find all single placements in big range speed (per second): %f" secondsTaken speedPerSecond


*)


