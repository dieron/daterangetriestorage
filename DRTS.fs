﻿namespace DateRangeTrieStorage

module DRTS =
    open Configurations
    open System
    open TypeDefinitions
    open System.Collections.Concurrent


    let intervalSize size :int64 = 
        match size with 
        | S1 -> 1L
        | S5 -> 5L
        | S15 -> 15L
        | S30 -> 30L
        | M1 -> 60L
        | M5 -> 60L * 5L
        | M15 -> 60L * 15L
        | M30 -> 60L * 30L
        | H1 -> 60L * 60L * 1L
        | H3 -> 60L * 60L * 3L
        | H6 -> 60L * 60L * 6L
        | H12 -> 60L * 60L * 12L
        | D1 -> 60L * 60L * 24L
        | D30 -> 60L * 60L * 24L* 30L
        | Global -> 1024L * 365L * 24L * 60L * 60L
        | Unknown -> 0L


    type TreeNode<'T>(beginningOfTime: DateTime, level: int, startInterval: int64, configuration: TrieStorageConfiguration) =
        let dateToSecondsInterval (date: DateTime) :int64 = 
            Math.Round((date - beginningOfTime).TotalSeconds) |> int64
        let secondsIntervalToDate (seconds: int64) :DateTime = 
            beginningOfTime + TimeSpan.FromSeconds ((float) seconds)
        let calculateEndInterval (startInterval : int64) size = startInterval + intervalSize size
        let intervalToPosition (interval: int64) (size: LevelConfigurationSize) :Position =
            interval / (intervalSize size)
        let positionToInterval (position: Position) (size: LevelConfigurationSize) :int64 =
            (intervalSize size) * position

        let mutable isComplete = false
        let lastLevel = configuration.Levels.[configuration.Levels.Length - 1].Level = level
        let size = configuration.Levels.[level].Size
        let childSize = if lastLevel then Unknown else configuration.Levels.[level + 1].Size
        let endInterval = calculateEndInterval startInterval size
        //let spanInterval = TimeSpan.FromSeconds ((float) (endInterval - startInterval))

        let mutable placements = new ConcurrentDictionary<'T, IPlacement<'T>>()
        let mutable children = new ConcurrentDictionary<Position, TreeNode<'T>>()

        member this.StartInterval = startInterval
        member this.EndInterval = endInterval

        member this.CheckComplete (now: DateTime) = isComplete <- if isComplete then true else (dateToSecondsInterval now) > endInterval
        member this.CheckCompleteAll (now: DateTime) = 
            let alreadyComplete = isComplete
            this.CheckComplete now 
            if not alreadyComplete then children.Values |> Seq.iter (fun x -> x.CheckCompleteAll now)

        member this.AddPlacement (placement: IPlacement<'T>) = 
            if lastLevel then do       
                placements.[placement.Id] <- placement

            else do
                let childNodePosition = intervalToPosition (dateToSecondsInterval placement.Date) childSize
                let createTreeNode = new Func<Position, TreeNode<'T>>(fun (position) -> new TreeNode<'T>(beginningOfTime, level + 1, positionToInterval position childSize, configuration)) 
                let node = children.GetOrAdd(childNodePosition, createTreeNode) 

                node.AddPlacement placement

                
        member this.FindPlacementNearTheTime id (expectedTime:DateTime) (deltaMs:float) :IPlacement<'T> option = 
            if lastLevel then 
                let found, value = placements.TryGetValue id 
                if found then Some value else None
            else
                let minExpectedInterval = dateToSecondsInterval (expectedTime.AddMilliseconds -deltaMs)
                let maxExpectedInterval = dateToSecondsInterval (expectedTime.AddMilliseconds deltaMs)

                this.FindPlacementBetweenIntervals id minExpectedInterval maxExpectedInterval

        member this.GetPlacementsNearTheTime (expectedTime:DateTime) (deltaMs:float) :seq<IPlacement<'T>> = 
            if lastLevel then 
                let filteredPlacements = placements.Values |> Seq.filter (fun x -> x.Date >= (expectedTime.AddMilliseconds -deltaMs) && x.Date <= (expectedTime.AddMilliseconds deltaMs)) 
                seq { for result in filteredPlacements -> result }
            else
                let minExpectedInterval = dateToSecondsInterval (expectedTime.AddMilliseconds -deltaMs)
                let maxExpectedInterval = dateToSecondsInterval (expectedTime.AddMilliseconds deltaMs)

                this.GetPlacementsBetweenIntervals minExpectedInterval maxExpectedInterval

        member this.FindPlacementInInterval id (minExpectedInterval:Int64) (maxExpectedInterval:Int64) :IPlacement<'T> option = 
            if lastLevel then 
                let found, value = placements.TryGetValue id 
                if found then Some value else None
            else
                this.FindPlacementBetweenIntervals id minExpectedInterval maxExpectedInterval

        member this.FindPlacementInDateRange id (minExpectedDate:DateTime) (maxExpectedDate:DateTime) :IPlacement<'T> option = 
            if lastLevel then 
                let found, value = placements.TryGetValue id 
                if found then Some value else None
            else
                this.FindPlacementBetweenIntervals id (dateToSecondsInterval minExpectedDate) (dateToSecondsInterval maxExpectedDate)

        member this.GetPlacementsInDateRange (minExpectedDate:DateTime) (maxExpectedDate:DateTime) :seq<IPlacement<'T>> = 
            if lastLevel then 
                let filteredPlacements = placements.Values |> Seq.filter (fun x -> x.Date >= minExpectedDate && x.Date <= maxExpectedDate) 
                seq { for result in filteredPlacements -> result }
            else
                this.GetPlacementsBetweenIntervals (dateToSecondsInterval minExpectedDate) (dateToSecondsInterval maxExpectedDate)

        member this.GetChildrenOrNone (p: Position) = 
            let found, value = children.TryGetValue p 
            if found then Some value else None

        member this.FindPlacementBetweenIntervals id minExpectedInterval maxExpectedInterval :IPlacement<'T> option =
            let minInterval = Math.Max(minExpectedInterval, startInterval)
            let maxInterval = Math.Min(maxExpectedInterval, endInterval)

            let minPosition = intervalToPosition minExpectedInterval childSize
            let maxPosition = intervalToPosition maxExpectedInterval childSize

            let posSeq = seq {minPosition..maxPosition}
            let filterNones seq = Seq.filter Option.isSome seq
            let matchingChildren = posSeq |> Seq.map (fun x -> this.GetChildrenOrNone x ) |> filterNones 
            let matchingPlacements = matchingChildren |> Seq.map (fun (x: TreeNode<'T> option) -> x.Value.FindPlacementInInterval id minInterval maxInterval) |> filterNones  
            let matchingPlacement = matchingPlacements |> Seq.tryHead
            if Option.isSome matchingPlacement then matchingPlacement.Value else None        

        member this.GetPlacementsBetweenIntervals minExpectedInterval maxExpectedInterval :seq<IPlacement<'T>> =
            if lastLevel then 
                let filteredPlacements = placements.Values |> Seq.filter (fun x -> dateToSecondsInterval x.Date >= minExpectedInterval && dateToSecondsInterval x.Date <= maxExpectedInterval) 
                filteredPlacements
            else
                let minInterval = Math.Max(minExpectedInterval, startInterval)
                let maxInterval = Math.Min(maxExpectedInterval, endInterval)

                let minPosition = intervalToPosition minInterval childSize
                let maxPosition = intervalToPosition maxInterval childSize

                let posSeq = seq {minPosition..maxPosition}
                let filterNones seq = Seq.filter Option.isSome seq
                let matchingChildren = posSeq |> Seq.map (fun x -> this.GetChildrenOrNone x ) |> filterNones 
                let matchingPlacements = matchingChildren |> Seq.map (fun (x: TreeNode<'T> option) -> x.Value.GetPlacementsBetweenIntervals minInterval maxInterval) 
                let result = Seq.collect (fun x -> x) matchingPlacements |> Seq.toList    
                seq result 


    type DataRangeTrieStorage<'T>(configuration: TrieStorageConfiguration, beginningOfTime: DateTime) =
        let rootNode = TreeNode<'T>(beginningOfTime, 0, 0L, configuration)
        member this.GetPlacementsInDateRange minExpectedDate maxExpectedDate :seq<IPlacement<'T>> = rootNode.GetPlacementsInDateRange minExpectedDate maxExpectedDate
        member this.FindPlacementInDateRange id minExpectedDate maxExpectedDate :IPlacement<'T> option = rootNode.FindPlacementInDateRange id minExpectedDate maxExpectedDate
        member  this.AddPlacement (placement: IPlacement<'T>) = rootNode.AddPlacement placement