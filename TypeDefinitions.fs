﻿namespace DateRangeTrieStorage

module TypeDefinitions =
    
    open System

    type LevelConfigurationSize = 
        | Global
        | D30
        | D1
        | H12
        | H6
        | H3
        | H1
        | M30
        | M15
        | M5
        | M1
        | S30
        | S15
        | S5
        | S1
        | Unknown

    type LevelConfiguration = {Level: int; Size: LevelConfigurationSize}

    type StorageConfiguration = LevelConfiguration array

    type Position = int64

    type Count = int

    type IPlacement<'T> = 
        abstract member Id: 'T with get, set            // Unique id of the interaction
        abstract member Date: DateTime with get, set    // Date the interaction happened in UTC
    


